alias sbt='docker run --rm -it -e TZ=Europe/Paris -v $PWD:/app -w /app -v cache:/root/.cache --name sbt swal4u/sbt:v1.3.7.5'
alias sbt-coverage='sbt clean coverage test coverageReport'
alias spark-submit='docker exec -it spark-master spark-submit --master spark://spark-master:7077 --executor-memory 2G'
alias spark-shell='docker exec -it spark-master spark-shell --master spark://spark-master:7077 --executor-memory 2G'
alias spark-start='docker run -d --rm -e TZ=Europe/Paris --net sparkCluster -p 4040:4040 -p 8080:8080 -p 8081:8081 -p 8090:8090 -v $PWD:/root -v notebook:/usr/local/zeppelin/notebook -v zeppelin:/usr/local/zeppelin/conf --name spark-master -h spark-master swal4u/spark-master:v2.3.0.3'
alias spark-stop='docker stop spark-master'
function slave-start () { docker run -d --rm --net sparkCluster -p "$2":8081 -v $PWD:/root --name "$1" -h "$1" swal4u/spark-slave:v2.3.0.1 ; }
function slave-stop () { docker stop "$1" ; }
alias zepp-start='docker exec -it spark-master zeppelin-daemon.sh start'
alias zepp-stop='docker exec -it spark-master zeppelin-daemon.sh stop'
alias zepp-status='docker exec -it spark-master zeppelin-daemon.sh status'
alias spark-sql='docker exec -it spark-master spark-sql --master spark://spark-master:7077'
function myip () { curl http://api.db-ip.com/v2/free/$(curl -s http://whatismyip.akamai.com)/"$1" ; }